Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Оценка оказаных услуг'
    },
    subtitle: {
        text: ''
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Количество оценок'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [
        {
            name: "Browsers",
            colorByPoint: true,
            data: [
                {
                    name: "Отлично",
                    y: 800,
                    drilldown: "Отлично",
                    color: '#14B668'
                },
                {
                    name: "Хорошо",
                    y: 200,
                    drilldown: "Хорошо",
                    color: '#F2994A'
                },
                {
                    name: "Плохо",
                    y: 70,
                    drilldown: "Плохо",
                    color: '#EB5757'
                }
            ]
        }
    ],

});